FROM gitlab-registry.cern.ch/linuxsupport/cs9-base
LABEL author="sebastien.joly@cern.ch"

RUN dnf -y install curl which bzip2 libgfortran git file man-db make \
                   gcc gcc-gfortran python3-pip python3-devel \
                   wget rpm nano locmap-release

RUN dnf -y install locmap

RUN locmap --enable kerberos
RUN locmap --enable afs
RUN locmap --enable eosclient
RUN locmap --configure kerberos; exit 0
RUN locmap --configure afs; exit 0
RUN locmap --configure eosclient; exit 0

RUN dnf -y install eos-xrootd xrootd xrootd-client

WORKDIR /usr/local/xsuite

RUN wget -q --progress=bar:force:noscroll https://github.com/conda-forge/miniforge/releases/download/4.14.0-0/Miniforge3-4.14.0-0-Linux-x86_64.sh
RUN bash Miniforge3-4.14.0-0-Linux-x86_64.sh -b -p /usr/local/xsuite/miniforge3
RUN rm Miniforge3-4.14.0-0-Linux-x86_64.sh

RUN source /usr/local/xsuite/miniforge3/bin/activate && \
    conda init && \
    conda create --name xsuite python=3.9 && \
    conda activate xsuite && \
    conda install pip && \
    conda install compilers=1.5.1

WORKDIR /usr/local/xsuite
RUN wget -q --progress=bar:force:noscroll https://developer.download.nvidia.com/compute/cuda/11.8.0/local_installers/cuda_11.8.0_520.61.05_linux.run
RUN bash cuda_11.8.0_520.61.05_linux.run
RUN rm cuda_11.8.0_520.61.05_linux.run

#RUN wget -q --progress=bar:force:noscroll https://developer.download.nvidia.com/compute/cuda/11.2.2/local_installers/cuda-repo-rhel8-11-2-local-11.2.2_460.32.03-1.x86_64.rpm
#RUN rpm -ih --quiet cuda-repo-rhel8-11-2-local-11.2.2_460.32.03-1.x86_64.rpm
#RUN dnf clean all
#RUN dnf -y --nobest module install cuda-drivers
#RUN dnf -y install cuda
